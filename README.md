# surfer-themes

This repository contains some themes for the [Surfer waveform viewer](https://surfer-project.org).

## Installation
To replace Surfer's default theme add the contents of the theme file to your user config file.

| Os      | Path                                                                  |
|---------|-----------------------------------------------------------------------|
| Linux   | `~/.config/surfer/config.toml`.                                       |
| Windows | `C:\Users\<Name>\AppData\Roaming\surfer-project\surfer\config.toml`.  |
| MacOs   | `/Users/<Name>/Library/Application Support/org.surfer-project.surfer` |

Surfer also allows having custom configs per directory. To use a theme in just a single directory add it to a file called 'surfer.toml' inside the directory.
